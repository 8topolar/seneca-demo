module.exports = ( options ) ->
  @add 'role:alias,cmd:repeat', ( msg, respond ) ->
    # respond null, { answer: 'hola mundo' }
    # @act "role:math,cmd:sum,left:#{ msg.n },right:#{ msg.times }", respond
    @act
      role: 'math'
      cmd: 'sum'
      left: msg.n
      right: msg.times
    , respond

  @wrap 'role:alias', ( msg, respond ) ->
    msg.n = Number( msg.n ).valueOf()
    msg.times = Number( msg.times ).valueOf()
    @prior msg, respond
