module.exports = ( options ) ->
  @add 'role:math,cmd:sum', ( msg, respond ) ->
    respond null, { answer: msg.left + msg.right }

  @add 'role:math,cmd:product', ( msg, respond ) ->
    respond null, { answer: msg.left * msg.right }

  @wrap 'role:math', ( msg, respond ) ->
    msg.left = Number( msg.left ).valueOf()
    msg.right = Number( msg.right ).valueOf()
    @prior msg, respond
